#!/bin/sh
#

SCRIPT_FILE=`readlink -f $0`
SCRIPT_PATH=`dirname $SCRIPT_FILE`

${SCRIPT_PATH}/make_db.sh

if [ $? -eq 1 ]; then
    exit 1
fi

echo "For make_virtual_host.sh"

sudo ${SCRIPT_PATH}/make_virtual_host.sh

if [ $? -eq 1 ]; then
    exit 1
fi

exit 0
