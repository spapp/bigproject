#!/bin/sh
#

SCRIPT_FILE=`readlink -f $0`
SCRIPT_PATH=`dirname $SCRIPT_FILE`

HOST_NAME="bigproject.loc"
VIRTUAL_HOST="${SCRIPT_PATH}/virtual_host"
DIR_SITE_AVAILABLE="/etc/apache2/sites-available"
DIR_SITE_ENABLED="/etc/apache2/sites-enabled"
HOSTS_FILE="/etc/hosts"

if [ "`id -u`" != "0" ]; then
    echo "This script (`basename ${0}`) must be run as root!"
    exit 1
fi

if [ -f ${DIR_SITE_AVAILABLE}/${HOST_NAME} ]; then
    echo "${DIR_SITE_AVAILABLE}/${HOST_NAME} is exists."
else
    cp -fv ${VIRTUAL_HOST} ${DIR_SITE_AVAILABLE}/${HOST_NAME}
fi

if [ -f ${DIR_SITE_AVAILABLE}/${HOST_NAME} ]; then
    if [ -h ${DIR_SITE_ENABLED}/${HOST_NAME} ]; then
        echo "Site is enabled."
    else
        ln -s ${DIR_SITE_AVAILABLE}/${HOST_NAME} ${DIR_SITE_ENABLED}/${HOST_NAME}
    fi
    /etc/init.d/apache2 reload
else
    exit 1
fi

if [ -z "`grep ${HOST_NAME} ${HOSTS_FILE}`" ]; then
    echo "\n127.0.0.1 ${HOST_NAME}" >> ${HOSTS_FILE}
fi

exit 0
