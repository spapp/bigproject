
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `u_nickname` varchar(255) NOT NULL,
  `u_firstname` varchar(255) NOT NULL,
  `u_lastname` varchar(255) NOT NULL,
  `u_email` varchar(255) NOT NULL,
  `u_password` varchar(32) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_u_nickname_index_idx` (`u_nickname`),
  UNIQUE KEY `user_u_email_index_idx` (`u_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

INSERT INTO `user`
    (`u_nickname`, `u_firstname`, `u_lastname`, `u_email`, `u_password`) 
VALUES 
	('aaa', 'aaa_fn', 'aaa_ln', 'aaa@aaa.com', MD5('aaa')), 
	('bbb', 'bbb_fn', 'bbb_ln', 'bbb@bbb.com', MD5('bbb'));