<?php
/**
 * Mysql database adapter
 */
class Db_Mysql_Adapter implements Db_Interface {
    /**
     * @var database pointer
     */
    protected $dbConnection = null;
    /**
     * @var array database config
     */
    protected $config = null;
    /**
     * @param Config|array database config
     * @return void
     */
    public function __construct($aConfig) {
        if ($aConfig instanceof Config) {
            $this->config = $aConfig;
        } elseif (is_array($aConfig)) {
            $this->config = new Config($aConfig);
        } else {
            throw new Exception('Invalid config format');
        }
        $this->connect();
    }
    /**
     * Connect to database.
     * @return boolean
     */
    public function connect() {
        $this->dbConnection = mysql_connect($this->config->host, $this->config->username, $this->config->password);
        if (!$this->dbConnection) {
            throw new Exception(mysql_error() , mysql_errno());
        }
        if (!mysql_select_db($this->config->dbname, $this->dbConnection)) {
            throw new Exception(mysql_error() , mysql_errno());
        }
        return (bool)$this->dbConnection;
    }
    /**
     * Close current database conection.
     * @return void
     */
    public function close() {
        if ($this->dbConnection) {
            mysql_close($this->dbConnection);
        }
    }
    /**
     * Escaping string.
     * @param string
     * @return string
     */
    public function escape($aString) {
        return mysql_real_escape_string((string)$aString, $this->dbConnection);
    }
    /**
     * @param string $aQuery SQL query
     * @return mixed
     */
    public function query($aQuery) {
        $resurce = mysql_query((string)$aQuery, $this->dbConnection);
        if (preg_match('~^SELECT~i', trim($aQuery))) {
            return new Db_Mysql_Recordset($resurce);
        } else {
            return $resurce;
        }
    }
}
?>