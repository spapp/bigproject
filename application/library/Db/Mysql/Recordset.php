<?php
/**
 * Mysql datbase recordset object
 */
class Db_Mysql_Recordset implements Countable, Iterator {
    /**
     * @var mysql query pointer
     */
    protected $resurce = null;
    /**
     * @var mixed current record
     */
    protected $currentItem = null;
    /**
     * @param mysql query pointer $aResurce
     * @return void
     */
    public function __construct($aResurce) {
        $this->resurce = $aResurce;
    }
    /**
     * Get record count
     * @return number
     */
    public function count() {
        if ($this->resurce) {
            return mysql_num_rows($this->resurce);
        }
        return 0;
    }
    /**
     * Get the current record.
     * @return mixed
     */
    public function current() {
        return $this->currentItem;
    }
    /**
     * @todo not implemented
     */
    public function key() {
        // FIXME
        
    }
    /**
     * Get next record.
     * @return boolean
     */
    public function next() {
        $this->currentItem = mysql_fetch_assoc($this->resurce);
        return (bool)$this->currentItem;
    }
    /**
     * @todo not implemented
     */
    public function rewind() {
        // FIXME
        
    }
    /**
     * The current recod validation.
     * @return boolean
     */
    public function valid() {
        return (bool)$this->currentItem;
    }
}
?>