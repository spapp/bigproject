<?php
/**
 * Interface for databse adapters.
 */
interface Db_Interface {
    /**
     * Connect to database.
     * @return boolean
     */
    public function connect();
    /**
     * Close current database conection.
     * @return void
     */
    public function close();
    /**
     * Escaping string.
     * @param string
     * @return string
     */
    public function escape($aString);
    /**
     * @param string $aQuery SQL query
     * @return mixed
     */
    public function query($aQuery);
}
?>