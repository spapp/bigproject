<?php
/**
 * Database table object
 */
class Db_Table {
    /**
     * @var mixed db object
     */
    protected $db = null;
    /**
     * @var string the table name
     */
    protected $name = null;
    /**
     * @param string table name
     * @param mixed db object
     */
    public function __construct($aTableName, $aDb) {
        $this->db = $aDb;
        $this->name = $aTableName;
    }
    /**
     * SQL insert
     * @param array values; key-value pairs
     * @return mixed
     */
    public function insert($aValues) {
        $keys = array();
        $values = array();
        $sql = array(
            'INSERT INTO ',
            '`',
            $this->name,
            '`'
        );
        foreach ($aValues as $key => $value) {
            array_push($keys, '`' . $key . '`');
            array_push($values, "'" . $this->db->escape($value) . "'");
        }
        array_push($sql, ' (', implode(', ', $keys) , ') ');
        array_push($sql, 'VALUES');
        array_push($sql, ' (', implode(', ', $values) , ')');
        return $this->db->query(implode('', $sql));
    }
    /**
     * SQL update
     * @todo not implemented
     * @return mixed
     */
    public function update() {
        // FIXME
        
    }
    /**
     * SQL delete
     * @todo not implemented
     * @return mixed
     */
    public function delete() {
        // FIXME
        
    }
    /**
     * SQL select
     * @param string sql query
     * @param array values [Optional] key-value pairs
     * @return mixed
     */
    public function select($aQuery, $aValues = null) {
        $sql = $aQuery;
        if (is_array($aValues)) {
            foreach ($aValues as $key => $value) {
                $sql = str_replace("%$key", "'" . $this->db->escape($value) . "'", $sql);
            }
        }
        return $this->db->query(trim($sql));
    }
}
?>