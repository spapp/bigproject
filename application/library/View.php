<?php
class View {
    /**
     * @var array variables container
     */
    protected $vars = array();
    /**
     * @var string layout path
     */
    protected $layoutPath = '';
    /**
     * @var string view path
     */
    protected $viewPath = '';
    public function __construct() {
        $this->layoutPath = APPLICATION_PATH . '/layouts/';
        $this->viewPath = APPLICATION_PATH . '/views/';
    }
    /**
     * Add a variable the view
     *
     * @see $this->vars
     * @param string $aName variable name
     * @param mixed $aValue variable value
     * @return void
     */
    public function __set($aName, $aValue) {
        $this->vars[$aName] = $aValue;
    }
    /**
     * Get a variable value
     *
     * @param string variable name
     * @return mixed
     */
    public function __get($aName) {
        if (array_key_exists($aName, $this->vars)) {
            return $this->vars[$aName];
        }
        return null;
    }
    /**
     * Get a css link
     * @todo not implemented
     * @return string a fix valua
     */
    public function css() {
        return '<link href="skins/default/main.css" type="text/css" rel="stylesheet" />';
    }
    /**
     * Render the layout.
     * @param string $aFile
     * @return void
     */
    public function renderLayout($aFile) {
        include $this->layoutPath . '/' . $aFile . '.php';
    }
    /**
     * Render a view.
     * @param string $aFile
     * @return void
     */
    public function render($aFile) {
        include $this->viewPath . '/' . $aFile . '.php';
    }
}
?>