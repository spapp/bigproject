<?php
class Controller {
    protected $view = null;
    /**
     * @var array apllication databases
     */
    protected $databases = array();
    public function __construct($aAction, $aDatabases) {
        $actionFn = $aAction . 'Action';
        $this->databases = $aDatabases;
        $this->view = new View();
        $this->init();
        if (method_exists($this, $actionFn)) {
            $this->{$actionFn}();
        }
        $this->view->renderLayout('layout');
    }
    public function init() {
    }
}
?>