<?php
/**
 * Static Loader class
 */
class Loader {
    protected function __constuct() {
    }
    /**
     * Initializate the application's autoload function.
     */
    public static function initAutoLoader() {
        spl_autoload_register('Loader::autoLoad');
    }
    /**
     * The application autoload function.
     * @param string $aName class, interface name
     * @return void
     */
    public static function autoLoad($aName) {
        $filename = str_replace('_', DIRECTORY_SEPARATOR, $aName) . '.php';
        include $filename;
        // FIXME: hibakezelés, ha mégse töltődne
        
    }
    /**
     * Load a file.
     *
     * @param string $aName
     * @return void
     */
    public static function loadFile($aName) {
        $filename = APPLICATION_PATH . '/' . $aName;
        include $filename;
        // FIXME: hibakezelés, ha mégse töltődne
        
    }
}
?>