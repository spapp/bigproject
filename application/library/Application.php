<?php
/**
 *
 */
class Application {
    /**
     * @var Config application config
     */
    protected $config = null;
    /**
     * @var array apllication databases
     */
    protected $databases = array();
    /**
     * @param string config file path
     * @return void
     */
    public function __construct($aConfig) {
        $this->config = new Config($aConfig);
        if ($this->config->database) {
            $this->_initDatabases($this->config->database->toArray());
        }
    }
    /**
     * init the aplication databases
     *
     * @param array databases configs
     * @return void
     */
    protected function _initDatabases($aDbConfig) {
        foreach ($aDbConfig as $name => $config) {
            $this->databases[$name] = Db::factory($config['adapter'], $config['params']);
        }
    }
    /**
     * run the aplication
     *
     * @return void
     */
    public function run() {
        $controllerName = 'DefaultController';
        include (APPLICATION_PATH . '/controllers/' . $controllerName . '.php');
        new $controllerName('default', $this->databases);
    }
}
?>