<?php
class DefaultController extends Controller {
    public function defaultAction() {
        Loader::loadFile('models/UserModel.php');
        $userModel = new UserModel($this->databases);
        $this->view->userList = $userModel->fetchAll();
    }
}
?>