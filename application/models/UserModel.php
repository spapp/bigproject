<?php
class UserModel {
    /**
     * @var array apllication databases
     */
    protected $databases = array();
    public function __construct($aDatabases) {
        $this->databases = $aDatabases;
    }
    public function fetchAll() {
        $table = new Db_Table('user', $this->getDb('slave'));
        $sql = 'SELECT
                    `user`.`id`,
                    `user`.`u_nickname`,
                    `user`.`u_firstname`,
                    `user`.`u_lastname`,
                    `user`.`u_email`
                FROM `user`';
        return $table->select($sql);
    }
    protected function getDb($aName = 'master') {
        if ($aName === 'slave') {
            $aName = 'slave' . time() % 2;
        }
        return $this->databases[$aName];
    }
}
?>