<?php
return array(
    'database' => array(
        // az első (master) adatbázis konfigja
        'master' => array(
            'adapter' => 'mysql',
            'params' => array(
                'host' => 'localhost',
                'port' => 3306,
                'username' => 'root',
                'password' => '123',
                'dbname' => 'bigproject'
            )
        ) ,
        // a második adatbázis konfigja
        'slave0' => array(
            'adapter' => 'mysql',
            'params' => array(
                'host' => 'localhost',
                'port' => 3306,
                'username' => 'root',
                'password' => '123',
                'dbname' => 'bigproject'
            )
        ) ,
        // a harmadik adatbázis konfigja
        'slave1' => array(
            'adapter' => 'mysql',
            'params' => array(
                'host' => 'localhost',
                'port' => 3306,
                'username' => 'root',
                'password' => '123',
                'dbname' => 'bigproject'
            )
        )
    )
);
?>
